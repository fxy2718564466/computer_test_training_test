import os,sqlite3,re
import numpy as np
import pandas as pd
from pyecharts import Bar,Pie

class ChipotleNan:

    def __init__(self):

        self.csv_name = r'D:\show\six\new-project-six\Frame\rem\chipotle.csv'

        self.df = pd.read_csv(self.csv_name, sep='\t')

    # csv 修改版
    def csv_to_info(self):

        ipr = self.df.quantity * (self.df.item_price.str.replace('$', '').astype('float64'))

        data_info = self.df.copy()

        data_info['ipr'] = ipr

        return data_info

    # 存至数据库

    def csv_to_db(self,file_name,sep=None):

        if sep == None:

            sep = ','

        else:

            sep = sep

        conn = sqlite3.connect(r"D:\show\six\new-project-six\Frame\db.sqlite3")

        df = pd.read_csv(file_name, sep=sep)

        df.to_sql(file_name.replace('.', '_'), conn)

    # 这段时间内销售个数最多的商品

    def the_most_number_shop(self):

        data_info = self.csv_to_info()

        data = data_info.groupby('item_name').sum()

        shop_name = data.sort_values(by='quantity').tail(1).index[0]

        return shop_name

    # 流水账

    def total_price(self):

        data_info = self.csv_to_info()

        all = data_info.ipr.sum()

        return all

    # 这段时间内销售金额最多的商品

    def The_shop_sold__period(self):

        data_info = self.csv_to_info()

        data = data_info.groupby('item_name').sum()

        shop_name = data.sort_values(by='ipr').tail(1).index[0]

        return shop_name

        # 每种商品的销售个数，并做图

    def one_of_shop_number_to_bar(self):

        data_info = self.csv_to_info()

        data = data_info.groupby('item_name').sum()

        axisx = data.index

        axisy = data.quantity

        from pyecharts import Bar

        bar = Bar('商品销售数量')

        bar.add('商家', axisx, axisy, is_lable_show=True, is_datazoom_show=True)

        bar.render('one.html')

        # 每种商品的销售金额，并做图

    def one_of_shop_money_to_bar(self):

        data_info = self.csv_to_info()

        data = data_info.groupby('item_name').sum()

        axisx = data.index

        axisy = data.ipr

        from pyecharts import Bar

        bar = Bar('每种商品销售金额')

        bar.add('商家', axisx, axisy, is_lable_show=True, is_datazoom_show=True)

        bar.render('two.html')

    # 每种商品销售金额在流水总额中所占的比例，做圆饼图

    def one_of_money_to_total_price_to_pie(self):

        data_info = self.csv_to_info()

        data = data_info.groupby('item_name').sum()

        axisx = data.index

        axisy = data.ipr

        pie = Pie("饼图示例")

        pie.add("每种商品销售金额在流水总额中所占的比例", axisx, axisy, is_label_show=True)

        pie.render('three.html')

        # 每个定单金额，并做bar图

    def order_id_money_to_bar(self):

        data_info = self.csv_to_info()

        data = data_info.groupby('order_id').sum()

        axisx = data.index

        axisy = data.ipr

        from pyecharts import Bar

        bar = Bar('每个订单金额')

        bar.add('订单', axisx, axisy, is_lable_show=True, is_datazoom_show=True)

        bar.render('four.html')

        # 产品“Chicken Bowl”,选择最多的配料

    def Chicken_Bowl_ingredients(self):

        data_info = self.csv_to_info()

        info = data_info[data_info.item_name.str.contains('Chicken Bowl')].choice_description

        infos = sorted(info, key=len, reverse=True)

        info = infos.pop(0).replace('[','').replace(']','')

        return info

        # 产品“Chicken Bowl”每种配料选择的次数，及所占比例，并做圆饼图

    def Chicken_Bowl_ingredient_to_bar(self):

        data_info = self.csv_to_info()

        bb = data_info[data_info.item_name.str.contains('Chicken Bowl')]

        cc = bb.groupby('choice_description').order_id.count()

        pie = Pie('每种配料选择的次数以及所占比例')

        pie.add('商家', cc.index, cc.values, is_label_show=True)

        pie.render('five.html')

    # 餐厅的产品的种类及对应的商品配料的价格，做出表格

    def Make_a_table_of_the_types_of_the_restaurant(self):

        data_info = self.csv_to_info()

        a = data_info.drop('order_id',axis=1)
        b = a.drop('quantity', axis=1)
        data = b.drop('ipr', axis=1).values

        return data

if __name__ == '__main__':

    x = ChipotleNan()

    info = x.Make_a_table_of_the_types_of_the_restaurant()

    print(info)


































































