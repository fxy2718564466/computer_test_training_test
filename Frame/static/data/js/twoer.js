var myChart_a3bddb8eecfc4a0a9882a52f466ccfcd = echarts.init(document.getElementById('a3bddb8eecfc4a0a9882a52f466ccfcd'), 'light', {renderer: 'canvas'});

var option_a3bddb8eecfc4a0a9882a52f466ccfcd = {
    "title": [
        {
            "text": "\u6bcf\u79cd\u5546\u54c1\u9500\u552e\u91d1\u989d",
            "left": "auto",
            "top": "auto",
            "textStyle": {
                "fontSize": 18
            },
            "subtextStyle": {
                "fontSize": 12
            }
        }
    ],
    "toolbox": {
        "show": true,
        "orient": "vertical",
        "left": "95%",
        "top": "center",
        "feature": {
            "saveAsImage": {
                "show": true,
                "title": "save as image"
            },
            "restore": {
                "show": true,
                "title": "restore"
            },
            "dataView": {
                "show": true,
                "title": "data view"
            }
        }
    },
    "series_id": 1204056,
    "tooltip": {
        "trigger": "item",
        "triggerOn": "mousemove|click",
        "axisPointer": {
            "type": "line"
        },
        "textStyle": {
            "fontSize": 14
        },
        "backgroundColor": "rgba(50,50,50,0.7)",
        "borderColor": "#333",
        "borderWidth": 0
    },
    "series": [
        {
            "type": "bar",
            "name": "\u5546\u5bb6",
            "data": [
                369.93000000000023,
                672.3600000000001,
                894.7500000000002,
                138.71,
                106.4,
                250.46,
                649.1799999999997,
                74.0,
                44.4,
                191.84000000000032,
                603.75,
                830.7100000000002,
                616.33,
                95.94,
                8.99,
                66.34,
                375.94,
                8044.629999999975,
                6387.059999999984,
                524.1100000000001,
                81.09,
                1506.25,
                1199.0100000000002,
                580.3399999999982,
                1033.9600000000003,
                2475.6199999999963,
                3.0,
                73.75000000000003,
                43.02,
                144.55,
                159.3,
                88.43,
                93.21,
                14.8,
                67.8,
                111.87,
                14.8,
                290.67999999999984,
                2479.8099999999995,
                4236.12999999999,
                375.32000000000005,
                35.66,
                391.1499999999997,
                554.5500000000001,
                901.9500000000002,
                1002.2700000000003,
                8.49,
                50.940000000000005,
                182.5,
                90.94
            ],
            "barCategoryGap": "20%",
            "label": {
                "normal": {
                    "show": false,
                    "position": "top",
                    "textStyle": {
                        "fontSize": 12
                    }
                },
                "emphasis": {
                    "show": true,
                    "textStyle": {
                        "fontSize": 12
                    }
                }
            },
            "markPoint": {
                "data": []
            },
            "markLine": {
                "data": []
            },
            "seriesId": 1204056
        }
    ],
    "legend": [
        {
            "data": [
                "\u5546\u5bb6"
            ],
            "selectedMode": "multiple",
            "show": true,
            "left": "center",
            "top": "top",
            "orient": "horizontal",
            "textStyle": {
                "fontSize": 12
            }
        }
    ],
    "animation": true,
    "xAxis": [
        {
            "show": true,
            "nameLocation": "middle",
            "nameGap": 25,
            "nameTextStyle": {
                "fontSize": 14
            },
            "axisTick": {
                "alignWithLabel": false
            },
            "inverse": false,
            "boundaryGap": true,
            "type": "category",
            "splitLine": {
                "show": false
            },
            "axisLine": {
                "lineStyle": {
                    "width": 1
                }
            },
            "axisLabel": {
                "interval": "auto",
                "rotate": 0,
                "margin": 8,
                "textStyle": {
                    "fontSize": 12
                }
            },
            "data": [
                "6 Pack Soft Drink",
                "Barbacoa Bowl",
                "Barbacoa Burrito",
                "Barbacoa Crispy Tacos",
                "Barbacoa Salad Bowl",
                "Barbacoa Soft Tacos",
                "Bottled Water",
                "Bowl",
                "Burrito",
                "Canned Soda",
                "Canned Soft Drink",
                "Carnitas Bowl",
                "Carnitas Burrito",
                "Carnitas Crispy Tacos",
                "Carnitas Salad",
                "Carnitas Salad Bowl",
                "Carnitas Soft Tacos",
                "Chicken Bowl",
                "Chicken Burrito",
                "Chicken Crispy Tacos",
                "Chicken Salad",
                "Chicken Salad Bowl",
                "Chicken Soft Tacos",
                "Chips",
                "Chips and Fresh Tomato Salsa",
                "Chips and Guacamole",
                "Chips and Mild Fresh Tomato Salsa",
                "Chips and Roasted Chili Corn Salsa",
                "Chips and Roasted Chili-Corn Salsa",
                "Chips and Tomatillo Green Chili Salsa",
                "Chips and Tomatillo Red Chili Salsa",
                "Chips and Tomatillo-Green Chili Salsa",
                "Chips and Tomatillo-Red Chili Salsa",
                "Crispy Tacos",
                "Izze",
                "Nantucket Nectar",
                "Salad",
                "Side of Chips",
                "Steak Bowl",
                "Steak Burrito",
                "Steak Crispy Tacos",
                "Steak Salad",
                "Steak Salad Bowl",
                "Steak Soft Tacos",
                "Veggie Bowl",
                "Veggie Burrito",
                "Veggie Crispy Tacos",
                "Veggie Salad",
                "Veggie Salad Bowl",
                "Veggie Soft Tacos"
            ]
        }
    ],
    "yAxis": [
        {
            "show": true,
            "nameLocation": "middle",
            "nameGap": 25,
            "nameTextStyle": {
                "fontSize": 14
            },
            "axisTick": {
                "alignWithLabel": false
            },
            "inverse": false,
            "boundaryGap": true,
            "type": "value",
            "splitLine": {
                "show": true
            },
            "axisLine": {
                "lineStyle": {
                    "width": 1
                }
            },
            "axisLabel": {
                "interval": "auto",
                "formatter": "{value} ",
                "rotate": 0,
                "margin": 8,
                "textStyle": {
                    "fontSize": 12
                }
            }
        }
    ],
    "color": [
        "#c23531",
        "#2f4554",
        "#61a0a8",
        "#d48265",
        "#749f83",
        "#ca8622",
        "#bda29a",
        "#6e7074",
        "#546570",
        "#c4ccd3",
        "#f05b72",
        "#ef5b9c",
        "#f47920",
        "#905a3d",
        "#fab27b",
        "#2a5caa",
        "#444693",
        "#726930",
        "#b2d235",
        "#6d8346",
        "#ac6767",
        "#1d953f",
        "#6950a1",
        "#918597",
        "#f6f5ec"
    ],
    "dataZoom": [
        {
            "show": true,
            "type": "slider",
            "start": 50,
            "end": 100,
            "orient": "horizontal"
        }
    ]
};
myChart_a3bddb8eecfc4a0a9882a52f466ccfcd.setOption(option_a3bddb8eecfc4a0a9882a52f466ccfcd);


