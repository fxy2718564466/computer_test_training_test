



var myChart_b5d5ec4525ae4c58a09a4cafc51f70ac = echarts.init(document.getElementById('b5d5ec4525ae4c58a09a4cafc51f70ac'), 'light', {renderer: 'canvas'});

var option_b5d5ec4525ae4c58a09a4cafc51f70ac = {
    "title": [
        {
            "text": "\u6bcf\u79cd\u914d\u6599\u9009\u62e9\u7684\u6b21\u6570\u4ee5\u53ca\u6240\u5360\u6bd4\u4f8b",
            "left": "auto",
            "top": "auto",
            "textStyle": {
                "fontSize": 18
            },
            "subtextStyle": {
                "fontSize": 12
            }
        }
    ],
    "toolbox": {
        "show": true,
        "orient": "vertical",
        "left": "95%",
        "top": "center",
        "feature": {
            "saveAsImage": {
                "show": true,
                "title": "save as image"
            },
            "restore": {
                "show": true,
                "title": "restore"
            },
            "dataView": {
                "show": true,
                "title": "data view"
            }
        }
    },
    "series_id": 6362965,
    "tooltip": {
        "trigger": "item",
        "triggerOn": "mousemove|click",
        "axisPointer": {
            "type": "line"
        },
        "textStyle": {
            "fontSize": 14
        },
        "backgroundColor": "rgba(50,50,50,0.7)",
        "borderColor": "#333",
        "borderWidth": 0
    },
    "series": [
        {
            "type": "pie",
            "name": "\u5546\u5bb6",
            "data": [
                {
                    "name": "[Fresh Tomato (Mild), [Guacamole, Rice]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato (Mild), [Lettuce, Fajita Veggies, Black Beans, Rice, Sour Cream, Cheese]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato (Mild), [Lettuce, Fajita Veggies, Pinto Beans, Rice, Sour Cream, Cheese]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato (Mild), [Rice, Sour Cream, Cheese]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), Cheese]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), Lettuce]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Black Beans, Rice, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Black Beans, Rice, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Black Beans, Rice, Cheese]]",
                    "value": 3.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                    "value": 5.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Black Beans, Rice, Fajita Veggies, Cheese]]",
                    "value": 2.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Black Beans, Rice, Fajita Veggies, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Black Beans, Rice, Guacamole, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Black Beans, Rice, Sour Cream, Guacamole]]",
                    "value": 2.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Pinto Beans, Rice, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Pinto Beans, Rice, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Pinto Beans, Rice, Cheese, Sour Cream]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Pinto Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Pinto Beans, Rice, Sour Cream, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Rice, Black Beans, Fajita Veggies, Cheese, Guacamole, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Rice, Black Beans, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Rice, Black Beans, Sour Cream, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Rice, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Rice, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Rice, Fajita Veggies, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Rice, Fajita Veggies, Sour Cream, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa (Mild), [Rice, Pinto Beans, Cheese, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, Cheese]",
                    "value": 3.0
                },
                {
                    "name": "[Fresh Tomato Salsa, Rice]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Black Beans, Cheese, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Black Beans, Cheese, Sour Cream]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Black Beans, Rice, Sour Cream, Cheese, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Cheese, Black Beans, Rice]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Cheese, Guacamole, Sour Cream, Fajita Vegetables, Rice]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Cheese, Rice, Black Beans]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Black Beans, Pinto Beans, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Pinto Beans, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Guacamole, Lettuce]]",
                    "value": 8.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 7.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Sour Cream, Lettuce]]",
                    "value": 9.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese]]",
                    "value": 3.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Guacamole, Lettuce]]",
                    "value": 3.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Pinto Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Pinto Beans, Cheese, Sour Cream, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Pinto Beans, Cheese, Sour Cream]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Sour Cream, Guacamole, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Sour Cream, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Sour Cream]]",
                    "value": 2.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Cheese, Guacamole]]",
                    "value": 2.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 9.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Cheese, Sour Cream, Guacamole]]",
                    "value": 3.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Cheese, Sour Cream, Lettuce]]",
                    "value": 3.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Guacamole, Lettuce]]",
                    "value": 3.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Lettuce]]",
                    "value": 10.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Pinto Beans, Cheese, Guacamole, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Pinto Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 5.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Pinto Beans, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Pinto Beans, Cheese]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Sour Cream, Guacamole, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Sour Cream, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Sour Cream, Lettuce]]",
                    "value": 4.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Fajita Vegetables, Rice]]",
                    "value": 17.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Guacamole, Cheese, Rice, Sour Cream, Fajita Vegetables]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Guacamole, Cheese, Sour Cream, Fajita Vegetables, Rice]]",
                    "value": 2.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Guacamole, Lettuce, Rice, Cheese, Sour Cream, Black Beans]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Guacamole, Sour Cream, Cheese, Rice, Fajita Vegetables]]",
                    "value": 2.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Lettuce, Cheese, Pinto Beans, Rice]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Lettuce, Fajita Vegetables, Guacamole, Rice, Black Beans]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Pinto Beans, Cheese, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Pinto Beans, Rice, Cheese, Lettuce, Guacamole, Sour Cream, Fajita Vegetables]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Pinto Beans, Rice, Lettuce, Cheese]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Cheese, Guacamole, Lettuce]]",
                    "value": 8.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Cheese, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Cheese, Lettuce, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Cheese, Lettuce]]",
                    "value": 5.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 14.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Cheese, Sour Cream, Guacamole]]",
                    "value": 8.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Cheese, Sour Cream, Lettuce]]",
                    "value": 14.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Cheese, Sour Cream]]",
                    "value": 11.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Cheese]]",
                    "value": 13.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Fajita Vegetables, Lettuce, Cheese, Sour Cream]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Guacamole, Lettuce]]",
                    "value": 3.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Guacamole]]",
                    "value": 2.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Lettuce, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Pinto Beans, Cheese, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Pinto Beans, Cheese, Sour Cream, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Pinto Beans, Cheese, Sour Cream]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Pinto Beans, Cheese]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Pinto Beans, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Sour Cream, Cheese, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Sour Cream, Cheese, Lettuce, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Sour Cream, Cheese]]",
                    "value": 3.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Sour Cream, Guacamole, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans, Sour Cream]]",
                    "value": 3.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Black Beans]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Cheese, Black Beans]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Cheese, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Cheese, Lettuce]]",
                    "value": 13.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Cheese, Sour Cream, Guacamole, Fajita Vegetables]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Cheese, Sour Cream, Guacamole]]",
                    "value": 5.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Cheese, Sour Cream, Lettuce]]",
                    "value": 13.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Cheese, Sour Cream]]",
                    "value": 10.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Cheese]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Fajita Vegetables, Black Beans, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Fajita Vegetables, Black Beans, Sour Cream, Cheese, Lettuce, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Fajita Vegetables, Black Beans, Sour Cream, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Fajita Vegetables, Black Beans]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Fajita Vegetables, Cheese, Lettuce, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Fajita Vegetables, Pinto Beans, Cheese, Lettuce, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Fajita Vegetables, Pinto Beans, Sour Cream, Cheese, Lettuce, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Fajita Vegetables, Sour Cream, Cheese, Guacamole]]",
                    "value": 4.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Fajita Vegetables]]",
                    "value": 8.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Pinto Beans, Cheese, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Pinto Beans, Cheese, Lettuce]]",
                    "value": 3.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Pinto Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Pinto Beans, Cheese, Sour Cream, Lettuce]]",
                    "value": 3.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Pinto Beans, Cheese]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Pinto Beans, Sour Cream, Cheese, Lettuce, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Pinto Beans, Sour Cream, Cheese, Lettuce]]",
                    "value": 4.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Sour Cream, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Rice, Sour Cream, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Sour Cream, Cheese]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Sour Cream, Fajita Vegetables, Rice, Guacamole, Cheese]]",
                    "value": 2.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Sour Cream, Guacamole, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Fresh Tomato Salsa, [Sour Cream, Lettuce, Rice, Cheese]]",
                    "value": 1.0
                },
                {
                    "name": "[Fresh Tomato Salsa]",
                    "value": 3.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa (Medium), Cheese]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa (Medium), [Black Beans, Fajita Veggies, Cheese, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa (Medium), [Black Beans, Rice, Cheese, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa (Medium), [Black Beans, Rice, Cheese, Sour Cream, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa (Medium), [Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa (Medium), [Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream]]",
                    "value": 3.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa (Medium), [Black Beans, Rice, Sour Cream, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa (Medium), [Pinto Beans, Black Beans, Rice, Cheese, Sour Cream, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa (Medium), [Pinto Beans, Black Beans, Rice, Fajita Veggies, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa (Medium), [Pinto Beans, Rice, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa (Medium), [Pinto Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa (Medium), [Rice, Cheese, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa (Medium), [Rice, Cheese]]",
                    "value": 2.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa (Medium), [Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa (Medium), [Rice, Fajita Veggies, Cheese]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa (Medium), [Rice, Fajita Veggies, Sour Cream]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Black Beans, Cheese, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Black Beans, Sour Cream, Cheese, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Cheese, Lettuce, Fajita Vegetables, Rice]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Black Beans, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Black Beans, Pinto Beans, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Lettuce, Black Beans]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Pinto Beans, Sour Cream, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 3.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Sour Cream]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Guacamole]]",
                    "value": 2.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Pinto Beans, Cheese, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Pinto Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Cheese, Sour Cream, Lettuce]]",
                    "value": 5.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Cheese, Sour Cream]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Pinto Beans, Cheese, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Pinto Beans, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Pinto Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Pinto Beans, Cheese, Sour Cream, Lettuce]]",
                    "value": 4.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Black Beans, Cheese, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Black Beans, Cheese, Guacamole]]",
                    "value": 4.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Black Beans, Cheese, Lettuce, Guacamole]]",
                    "value": 2.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Black Beans, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Black Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 4.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Black Beans, Cheese, Sour Cream, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Black Beans, Cheese, Sour Cream, Lettuce]]",
                    "value": 10.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Black Beans, Cheese, Sour Cream]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Black Beans, Cheese]]",
                    "value": 3.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Black Beans, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Black Beans, Guacamole]]",
                    "value": 2.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Black Beans, Sour Cream, Cheese, Guacamole]]",
                    "value": 7.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Black Beans, Sour Cream, Cheese, Lettuce, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Black Beans, Sour Cream, Guacamole]]",
                    "value": 3.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Black Beans, Sour Cream]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Cheese, Lettuce]]",
                    "value": 3.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Cheese, Sour Cream, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Cheese, Sour Cream, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Cheese]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Fajita Vegetables, Black Beans, Sour Cream, Cheese, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Fajita Vegetables, Cheese, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Fajita Vegetables, Lettuce, Pinto Beans, Black Beans, Guacamole, Cheese]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Pinto Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Pinto Beans, Cheese, Sour Cream, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Pinto Beans, Cheese, Sour Cream, Lettuce]]",
                    "value": 4.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Pinto Beans, Lettuce, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Roasted Chili Corn Salsa, [Rice, Pinto Beans, Sour Cream, Cheese, Guacamole]]",
                    "value": 2.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Fajita Vegetables, Black Beans, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Fajita Vegetables, Rice, Black Beans]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Fajita Vegetables, Rice, Cheese, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Fajita Vegetables, Rice, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Fajita Vegetables, Rice, Pinto Beans, Cheese, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Fajita Vegetables, Rice, Pinto Beans, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Fajita Vegetables, Rice, Sour Cream, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Fajita Vegetables, Rice]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Black Beans, Cheese, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Black Beans, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Black Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Black Beans, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Black Beans, Cheese, Sour Cream]]",
                    "value": 2.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Black Beans, Cheese]]",
                    "value": 3.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Black Beans, Guacamole, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Black Beans, Pinto Beans, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Black Beans, Sour Cream, Cheese]]",
                    "value": 2.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Black Beans, Sour Cream, Lettuce]]",
                    "value": 3.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Black Beans]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Cheese, Guacamole, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Cheese, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Cheese, Sour Cream, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Cheese, Sour Cream, Lettuce]]",
                    "value": 4.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Cheese]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Fajita Vegetables, Black Beans, Sour Cream, Cheese, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Pinto Beans, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Pinto Beans, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Pinto Beans, Sour Cream, Lettuce]]",
                    "value": 12.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Sour Cream, Cheese, Guacamole]]",
                    "value": 4.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Sour Cream, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Rice, Sour Cream, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Green Chili Salsa, [Sour Cream, Cheese, Guacamole]]",
                    "value": 2.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, Fajita Vegetables]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, Rice]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Black Beans, Cheese, Sour Cream]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Fajita Vegetables, Black Beans, Sour Cream, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 3.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Sour Cream]]",
                    "value": 3.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Fajita Vegetables, Rice, Cheese, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Rice, Black Beans, Cheese, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Rice, Black Beans, Cheese, Lettuce, Guacamole]]",
                    "value": 2.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Rice, Black Beans, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Rice, Black Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 5.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Rice, Black Beans, Cheese, Sour Cream, Lettuce]]",
                    "value": 8.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Rice, Black Beans, Cheese, Sour Cream]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Rice, Black Beans, Pinto Beans, Cheese, Sour Cream, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Rice, Black Beans, Pinto Beans, Cheese, Sour Cream]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Rice, Black Beans, Sour Cream, Cheese, Lettuce, Guacamole]]",
                    "value": 2.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Rice, Black Beans, Sour Cream, Cheese, Lettuce]]",
                    "value": 7.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Rice, Black Beans, Sour Cream, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Rice, Black Beans]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Rice, Cheese, Lettuce]]",
                    "value": 3.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Rice, Cheese, Sour Cream, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Rice, Cheese, Sour Cream, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Rice, Cheese]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Rice, Fajita Vegetables, Black Beans, Cheese, Lettuce, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Rice, Fajita Vegetables, Black Beans, Sour Cream, Cheese]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Rice, Fajita Vegetables, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Rice, Pinto Beans, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Rice, Pinto Beans, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Rice, Sour Cream, Cheese, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa, [Sour Cream, Cheese, Guacamole, Lettuce, Black Beans, Rice]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo Red Chili Salsa]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo-Green Chili Salsa (Medium), Black Beans]",
                    "value": 2.0
                },
                {
                    "name": "[Tomatillo-Green Chili Salsa (Medium), [Black Beans, Rice, Cheese, Sour Cream]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo-Green Chili Salsa (Medium), [Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo-Green Chili Salsa (Medium), [Fajita Veggies, Cheese, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo-Green Chili Salsa (Medium), [Pinto Beans, Black Beans, Rice, Fajita Veggies, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo-Green Chili Salsa (Medium), [Pinto Beans, Rice, Fajita Veggies, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo-Green Chili Salsa (Medium), [Rice, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo-Red Chili Salsa (Hot), Rice]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo-Red Chili Salsa (Hot), [Black Beans, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo-Red Chili Salsa (Hot), [Black Beans, Rice, Cheese, Lettuce]]",
                    "value": 9.0
                },
                {
                    "name": "[Tomatillo-Red Chili Salsa (Hot), [Black Beans, Rice, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo-Red Chili Salsa (Hot), [Black Beans, Rice, Cheese, Sour Cream]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo-Red Chili Salsa (Hot), [Black Beans, Rice, Fajita Veggies, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo-Red Chili Salsa (Hot), [Cheese, Lettuce, Rice, Black Beans]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo-Red Chili Salsa (Hot), [Cheese, Sour Cream, Fajita Veggies, Pinto Beans, Rice]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo-Red Chili Salsa (Hot), [Pinto Beans, Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo-Red Chili Salsa (Hot), [Pinto Beans, Cheese, Sour Cream, Rice, Fajita Veggies, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo-Red Chili Salsa (Hot), [Pinto Beans, Rice, Cheese, Sour Cream, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo-Red Chili Salsa (Hot), [Pinto Beans, Rice, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo-Red Chili Salsa (Hot), [Pinto Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Guacamole]]",
                    "value": 3.0
                },
                {
                    "name": "[Tomatillo-Red Chili Salsa (Hot), [Pinto Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo-Red Chili Salsa (Hot), [Pinto Beans, Rice, Fajita Veggies, Cheese, Sour Cream]]",
                    "value": 2.0
                },
                {
                    "name": "[Tomatillo-Red Chili Salsa (Hot), [Rice, Black Beans, Cheese, Lettuce]]",
                    "value": 3.0
                },
                {
                    "name": "[Tomatillo-Red Chili Salsa (Hot), [Rice, Cheese, Sour Cream, Lettuce]]",
                    "value": 3.0
                },
                {
                    "name": "[Tomatillo-Red Chili Salsa (Hot), [Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[Tomatillo-Red Chili Salsa (Hot), [Rice, Fajita Veggies, Pinto Beans, Sour Cream, Cheese, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo-Red Chili Salsa (Hot), [Rice, Fajita Veggies, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo-Red Chili Salsa (Hot), [Rice, Pinto Beans, Fajita Veggies, Cheese, Sour Cream, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[Tomatillo-Red Chili Salsa (Hot), [Sour Cream, Cheese, Fajita Veggies, Guacamole, Rice, Pinto Beans]]",
                    "value": 1.0
                },
                {
                    "name": "[White Rice]",
                    "value": 2.0
                },
                {
                    "name": "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium), Tomatillo-Red Chili Salsa (Hot)], [Black Beans, Rice, Cheese, Sour Cream]]",
                    "value": 1.0
                },
                {
                    "name": "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium), Tomatillo-Red Chili Salsa (Hot)], [Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium), Tomatillo-Red Chili Salsa (Hot)], [Pinto Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium)], [Black Beans, Cheese, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium)], [Black Beans, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium)], [Black Beans, Rice, Cheese, Sour Cream, Lettuce]]",
                    "value": 3.0
                },
                {
                    "name": "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium)], [Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium)], [Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium)], [Black Beans, Rice, Fajita Veggies, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium)], [Fajita Veggies, Rice]]",
                    "value": 1.0
                },
                {
                    "name": "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium)], [Rice, Cheese, Sour Cream, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium)], [Rice, Fajita Veggies]]",
                    "value": 4.0
                },
                {
                    "name": "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium)], [Rice, Sour Cream, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[[Fresh Tomato Salsa (Mild), Tomatillo-Green Chili Salsa (Medium), Roasted Chili Corn Salsa (Medium), Tomatillo-Red Chili Salsa (Hot)], [Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream]]",
                    "value": 1.0
                },
                {
                    "name": "[[Fresh Tomato Salsa (Mild), Tomatillo-Green Chili Salsa (Medium), Roasted Chili Corn Salsa (Medium)], [Black Beans, Rice, Fajita Veggies, Cheese, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[[Fresh Tomato Salsa (Mild), Tomatillo-Green Chili Salsa (Medium)], [Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[[Fresh Tomato Salsa (Mild), Tomatillo-Red Chili Salsa (Hot)], [Black Beans, Rice, Cheese, Sour Cream, Guacamole]]",
                    "value": 3.0
                },
                {
                    "name": "[[Fresh Tomato Salsa (Mild), Tomatillo-Red Chili Salsa (Hot)], [Black Beans, Rice, Cheese, Sour Cream]]",
                    "value": 7.0
                },
                {
                    "name": "[[Fresh Tomato Salsa (Mild), Tomatillo-Red Chili Salsa (Hot)], [Guacamole, Rice, Black Beans, Cheese, Sour Cream]]",
                    "value": 1.0
                },
                {
                    "name": "[[Roasted Chili Corn Salsa (Medium), Fresh Tomato Salsa (Mild)], [Fajita Veggies, Lettuce, Rice, Black Beans]]",
                    "value": 1.0
                },
                {
                    "name": "[[Roasted Chili Corn Salsa (Medium), Fresh Tomato Salsa (Mild)], [Rice, Black Beans, Fajita Veggies, Cheese, Sour Cream, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[[Roasted Chili Corn Salsa (Medium), Fresh Tomato Salsa (Mild)], [Rice, Black Beans, Fajita Veggies, Guacamole]]",
                    "value": 2.0
                },
                {
                    "name": "[[Roasted Chili Corn Salsa (Medium), Fresh Tomato Salsa (Mild)], [Rice, Fajita Veggies]]",
                    "value": 5.0
                },
                {
                    "name": "[[Roasted Chili Corn Salsa (Medium), Tomatillo-Green Chili Salsa (Medium)], [Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[[Roasted Chili Corn Salsa (Medium), Tomatillo-Red Chili Salsa (Hot), Fresh Tomato Salsa (Mild), Tomatillo-Green Chili Salsa (Medium)], [Rice, Black Beans, Fajita Veggies, Cheese, Sour Cream]]",
                    "value": 1.0
                },
                {
                    "name": "[[Roasted Chili Corn Salsa (Medium), Tomatillo-Red Chili Salsa (Hot), Fresh Tomato Salsa (Mild)], [Rice, Pinto Beans, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                    "value": 3.0
                },
                {
                    "name": "[[Roasted Chili Corn Salsa (Medium), Tomatillo-Red Chili Salsa (Hot)], [Black Beans, Rice, Fajita Veggies, Guacamole, Lettuce]]",
                    "value": 2.0
                },
                {
                    "name": "[[Roasted Chili Corn Salsa (Medium), Tomatillo-Red Chili Salsa (Hot)], [Pinto Beans, Rice, Fajita Veggies, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[[Roasted Chili Corn Salsa (Medium), Tomatillo-Red Chili Salsa (Hot)], [Pinto Beans, Rice, Fajita Veggies, Cheese]]",
                    "value": 1.0
                },
                {
                    "name": "[[Roasted Chili Corn Salsa (Medium), Tomatillo-Red Chili Salsa (Hot)], [Pinto Beans, Rice, Fajita Veggies]]",
                    "value": 1.0
                },
                {
                    "name": "[[Tomatillo-Green Chili Salsa (Medium), Roasted Chili Corn Salsa (Medium)], [Black Beans, Rice, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[[Tomatillo-Green Chili Salsa (Medium), Roasted Chili Corn Salsa (Medium)], [Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[[Tomatillo-Green Chili Salsa (Medium), Roasted Chili Corn Salsa (Medium)], [Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[[Tomatillo-Green Chili Salsa (Medium), Roasted Chili Corn Salsa (Medium)], [Rice, Fajita Veggies, Guacamole, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[[Tomatillo-Green Chili Salsa (Medium), Tomatillo-Red Chili Salsa (Hot)], [Pinto Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Guacamole]]",
                    "value": 1.0
                },
                {
                    "name": "[[Tomatillo-Red Chili Salsa (Hot), Tomatillo-Green Chili Salsa (Medium)], [Rice, Black Beans, Cheese, Lettuce]]",
                    "value": 1.0
                },
                {
                    "name": "[[Tomatillo-Red Chili Salsa (Hot), Tomatillo-Green Chili Salsa (Medium)], [Rice, Black Beans, Lettuce]]",
                    "value": 1.0
                }
            ],
            "radius": [
                "0%",
                "75%"
            ],
            "center": [
                "50%",
                "50%"
            ],
            "label": {
                "normal": {
                    "show": true,
                    "position": "outside",
                    "textStyle": {
                        "fontSize": 12
                    },
                    "formatter": "{b}: {d}%"
                },
                "emphasis": {
                    "show": true,
                    "textStyle": {
                        "fontSize": 12
                    },
                    "formatter": "{b}: {d}%"
                }
            },
            "seriesId": 6362965
        }
    ],
    "legend": [
        {
            "data": [
                "[Fresh Tomato (Mild), [Guacamole, Rice]]",
                "[Fresh Tomato (Mild), [Lettuce, Fajita Veggies, Black Beans, Rice, Sour Cream, Cheese]]",
                "[Fresh Tomato (Mild), [Lettuce, Fajita Veggies, Pinto Beans, Rice, Sour Cream, Cheese]]",
                "[Fresh Tomato (Mild), [Rice, Sour Cream, Cheese]]",
                "[Fresh Tomato Salsa (Mild), Cheese]",
                "[Fresh Tomato Salsa (Mild), Lettuce]",
                "[Fresh Tomato Salsa (Mild), [Black Beans, Rice, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa (Mild), [Black Beans, Rice, Cheese, Sour Cream, Lettuce]]",
                "[Fresh Tomato Salsa (Mild), [Black Beans, Rice, Cheese]]",
                "[Fresh Tomato Salsa (Mild), [Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa (Mild), [Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                "[Fresh Tomato Salsa (Mild), [Black Beans, Rice, Fajita Veggies, Cheese]]",
                "[Fresh Tomato Salsa (Mild), [Black Beans, Rice, Fajita Veggies, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa (Mild), [Black Beans, Rice, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa (Mild), [Black Beans, Rice, Sour Cream, Guacamole]]",
                "[Fresh Tomato Salsa (Mild), [Pinto Beans, Rice, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa (Mild), [Pinto Beans, Rice, Cheese, Sour Cream, Lettuce]]",
                "[Fresh Tomato Salsa (Mild), [Pinto Beans, Rice, Cheese, Sour Cream]]",
                "[Fresh Tomato Salsa (Mild), [Pinto Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa (Mild), [Pinto Beans, Rice, Sour Cream, Guacamole]]",
                "[Fresh Tomato Salsa (Mild), [Rice, Black Beans, Fajita Veggies, Cheese, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa (Mild), [Rice, Black Beans, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                "[Fresh Tomato Salsa (Mild), [Rice, Black Beans, Sour Cream, Guacamole]]",
                "[Fresh Tomato Salsa (Mild), [Rice, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa (Mild), [Rice, Cheese, Sour Cream, Lettuce]]",
                "[Fresh Tomato Salsa (Mild), [Rice, Fajita Veggies, Cheese, Lettuce]]",
                "[Fresh Tomato Salsa (Mild), [Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                "[Fresh Tomato Salsa (Mild), [Rice, Fajita Veggies, Sour Cream, Guacamole]]",
                "[Fresh Tomato Salsa (Mild), [Rice, Pinto Beans, Cheese, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, Cheese]",
                "[Fresh Tomato Salsa, Rice]",
                "[Fresh Tomato Salsa, [Black Beans, Cheese, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Black Beans, Cheese, Sour Cream]]",
                "[Fresh Tomato Salsa, [Black Beans, Rice, Sour Cream, Cheese, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Cheese, Black Beans, Rice]]",
                "[Fresh Tomato Salsa, [Cheese, Guacamole, Sour Cream, Fajita Vegetables, Rice]]",
                "[Fresh Tomato Salsa, [Cheese, Rice, Black Beans]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Black Beans, Pinto Beans, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Pinto Beans, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Guacamole]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Sour Cream, Lettuce]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Pinto Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Pinto Beans, Cheese, Sour Cream, Guacamole]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Pinto Beans, Cheese, Sour Cream]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Sour Cream, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Sour Cream, Lettuce]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Black Beans, Sour Cream]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Cheese, Guacamole]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Cheese, Lettuce]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Cheese, Sour Cream, Guacamole]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Cheese, Sour Cream, Lettuce]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Lettuce]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Pinto Beans, Cheese, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Pinto Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Pinto Beans, Cheese, Sour Cream, Lettuce]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Pinto Beans, Cheese]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Sour Cream, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Sour Cream, Guacamole]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice, Sour Cream, Lettuce]]",
                "[Fresh Tomato Salsa, [Fajita Vegetables, Rice]]",
                "[Fresh Tomato Salsa, [Guacamole, Cheese, Rice, Sour Cream, Fajita Vegetables]]",
                "[Fresh Tomato Salsa, [Guacamole, Cheese, Sour Cream, Fajita Vegetables, Rice]]",
                "[Fresh Tomato Salsa, [Guacamole, Lettuce, Rice, Cheese, Sour Cream, Black Beans]]",
                "[Fresh Tomato Salsa, [Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Guacamole, Sour Cream, Cheese, Rice, Fajita Vegetables]]",
                "[Fresh Tomato Salsa, [Lettuce, Cheese, Pinto Beans, Rice]]",
                "[Fresh Tomato Salsa, [Lettuce, Fajita Vegetables, Guacamole, Rice, Black Beans]]",
                "[Fresh Tomato Salsa, [Pinto Beans, Cheese, Guacamole]]",
                "[Fresh Tomato Salsa, [Pinto Beans, Rice, Cheese, Lettuce, Guacamole, Sour Cream, Fajita Vegetables]]",
                "[Fresh Tomato Salsa, [Pinto Beans, Rice, Lettuce, Cheese]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Cheese, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Cheese, Guacamole]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Cheese, Lettuce, Guacamole]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Cheese, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Cheese, Sour Cream, Guacamole]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Cheese, Sour Cream, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Cheese, Sour Cream]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Cheese]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Fajita Vegetables, Lettuce, Cheese, Sour Cream]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Guacamole]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Lettuce, Guacamole]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Pinto Beans, Cheese, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Pinto Beans, Cheese, Sour Cream, Guacamole]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Pinto Beans, Cheese, Sour Cream]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Pinto Beans, Cheese]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Pinto Beans, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Sour Cream, Cheese, Guacamole]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Sour Cream, Cheese, Lettuce, Guacamole]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Sour Cream, Cheese]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Sour Cream, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans, Sour Cream]]",
                "[Fresh Tomato Salsa, [Rice, Black Beans]]",
                "[Fresh Tomato Salsa, [Rice, Cheese, Black Beans]]",
                "[Fresh Tomato Salsa, [Rice, Cheese, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Cheese, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Cheese, Sour Cream, Guacamole, Fajita Vegetables]]",
                "[Fresh Tomato Salsa, [Rice, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Cheese, Sour Cream, Guacamole]]",
                "[Fresh Tomato Salsa, [Rice, Cheese, Sour Cream, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Cheese, Sour Cream]]",
                "[Fresh Tomato Salsa, [Rice, Cheese]]",
                "[Fresh Tomato Salsa, [Rice, Fajita Vegetables, Black Beans, Cheese, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Fajita Vegetables, Black Beans, Sour Cream, Cheese, Lettuce, Guacamole]]",
                "[Fresh Tomato Salsa, [Rice, Fajita Vegetables, Black Beans, Sour Cream, Cheese, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Fajita Vegetables, Black Beans]]",
                "[Fresh Tomato Salsa, [Rice, Fajita Vegetables, Cheese, Lettuce, Guacamole]]",
                "[Fresh Tomato Salsa, [Rice, Fajita Vegetables, Pinto Beans, Cheese, Lettuce, Guacamole]]",
                "[Fresh Tomato Salsa, [Rice, Fajita Vegetables, Pinto Beans, Sour Cream, Cheese, Lettuce, Guacamole]]",
                "[Fresh Tomato Salsa, [Rice, Fajita Vegetables, Sour Cream, Cheese, Guacamole]]",
                "[Fresh Tomato Salsa, [Rice, Fajita Vegetables]]",
                "[Fresh Tomato Salsa, [Rice, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Guacamole]]",
                "[Fresh Tomato Salsa, [Rice, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Pinto Beans, Cheese, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Pinto Beans, Cheese, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Pinto Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Pinto Beans, Cheese, Sour Cream, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Pinto Beans, Cheese]]",
                "[Fresh Tomato Salsa, [Rice, Pinto Beans, Sour Cream, Cheese, Lettuce, Guacamole]]",
                "[Fresh Tomato Salsa, [Rice, Pinto Beans, Sour Cream, Cheese, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Sour Cream, Cheese, Lettuce]]",
                "[Fresh Tomato Salsa, [Rice, Sour Cream, Guacamole]]",
                "[Fresh Tomato Salsa, [Sour Cream, Cheese]]",
                "[Fresh Tomato Salsa, [Sour Cream, Fajita Vegetables, Rice, Guacamole, Cheese]]",
                "[Fresh Tomato Salsa, [Sour Cream, Guacamole, Lettuce]]",
                "[Fresh Tomato Salsa, [Sour Cream, Lettuce, Rice, Cheese]]",
                "[Fresh Tomato Salsa]",
                "[Roasted Chili Corn Salsa (Medium), Cheese]",
                "[Roasted Chili Corn Salsa (Medium), [Black Beans, Fajita Veggies, Cheese, Guacamole, Lettuce]]",
                "[Roasted Chili Corn Salsa (Medium), [Black Beans, Rice, Cheese, Guacamole, Lettuce]]",
                "[Roasted Chili Corn Salsa (Medium), [Black Beans, Rice, Cheese, Sour Cream, Lettuce]]",
                "[Roasted Chili Corn Salsa (Medium), [Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Roasted Chili Corn Salsa (Medium), [Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream]]",
                "[Roasted Chili Corn Salsa (Medium), [Black Beans, Rice, Sour Cream, Guacamole]]",
                "[Roasted Chili Corn Salsa (Medium), [Pinto Beans, Black Beans, Rice, Cheese, Sour Cream, Guacamole]]",
                "[Roasted Chili Corn Salsa (Medium), [Pinto Beans, Black Beans, Rice, Fajita Veggies, Cheese, Lettuce]]",
                "[Roasted Chili Corn Salsa (Medium), [Pinto Beans, Rice, Cheese, Sour Cream, Lettuce]]",
                "[Roasted Chili Corn Salsa (Medium), [Pinto Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                "[Roasted Chili Corn Salsa (Medium), [Rice, Cheese, Lettuce]]",
                "[Roasted Chili Corn Salsa (Medium), [Rice, Cheese]]",
                "[Roasted Chili Corn Salsa (Medium), [Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                "[Roasted Chili Corn Salsa (Medium), [Rice, Fajita Veggies, Cheese]]",
                "[Roasted Chili Corn Salsa (Medium), [Rice, Fajita Veggies, Sour Cream]]",
                "[Roasted Chili Corn Salsa, [Black Beans, Cheese, Guacamole]]",
                "[Roasted Chili Corn Salsa, [Black Beans, Sour Cream, Cheese, Guacamole]]",
                "[Roasted Chili Corn Salsa, [Cheese, Lettuce, Fajita Vegetables, Rice]]",
                "[Roasted Chili Corn Salsa, [Cheese, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Black Beans, Guacamole]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Black Beans, Pinto Beans, Cheese, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Cheese, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Lettuce, Black Beans]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Pinto Beans, Sour Cream, Guacamole]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Guacamole, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Guacamole]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Sour Cream]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Guacamole, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Guacamole]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Pinto Beans, Cheese, Guacamole, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Pinto Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Black Beans, Sour Cream, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Cheese, Sour Cream, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Cheese, Sour Cream]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Pinto Beans, Cheese, Guacamole, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Pinto Beans, Cheese, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Pinto Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Pinto Beans, Cheese, Sour Cream, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice, Sour Cream, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Fajita Vegetables, Rice]]",
                "[Roasted Chili Corn Salsa, [Rice, Black Beans, Cheese, Guacamole, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Rice, Black Beans, Cheese, Guacamole]]",
                "[Roasted Chili Corn Salsa, [Rice, Black Beans, Cheese, Lettuce, Guacamole]]",
                "[Roasted Chili Corn Salsa, [Rice, Black Beans, Cheese, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Rice, Black Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Rice, Black Beans, Cheese, Sour Cream, Guacamole]]",
                "[Roasted Chili Corn Salsa, [Rice, Black Beans, Cheese, Sour Cream, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Rice, Black Beans, Cheese, Sour Cream]]",
                "[Roasted Chili Corn Salsa, [Rice, Black Beans, Cheese]]",
                "[Roasted Chili Corn Salsa, [Rice, Black Beans, Guacamole, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Rice, Black Beans, Guacamole]]",
                "[Roasted Chili Corn Salsa, [Rice, Black Beans, Sour Cream, Cheese, Guacamole]]",
                "[Roasted Chili Corn Salsa, [Rice, Black Beans, Sour Cream, Cheese, Lettuce, Guacamole]]",
                "[Roasted Chili Corn Salsa, [Rice, Black Beans, Sour Cream, Guacamole]]",
                "[Roasted Chili Corn Salsa, [Rice, Black Beans, Sour Cream]]",
                "[Roasted Chili Corn Salsa, [Rice, Cheese, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Rice, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Rice, Cheese, Sour Cream, Guacamole]]",
                "[Roasted Chili Corn Salsa, [Rice, Cheese, Sour Cream, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Rice, Cheese]]",
                "[Roasted Chili Corn Salsa, [Rice, Fajita Vegetables, Black Beans, Sour Cream, Cheese, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Rice, Fajita Vegetables, Cheese, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Rice, Fajita Vegetables, Lettuce, Pinto Beans, Black Beans, Guacamole, Cheese]]",
                "[Roasted Chili Corn Salsa, [Rice, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Rice, Pinto Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Rice, Pinto Beans, Cheese, Sour Cream, Guacamole]]",
                "[Roasted Chili Corn Salsa, [Rice, Pinto Beans, Cheese, Sour Cream, Lettuce]]",
                "[Roasted Chili Corn Salsa, [Rice, Pinto Beans, Lettuce, Guacamole]]",
                "[Roasted Chili Corn Salsa, [Rice, Pinto Beans, Sour Cream, Cheese, Guacamole]]",
                "[Tomatillo Green Chili Salsa, [Fajita Vegetables, Black Beans, Sour Cream, Lettuce]]",
                "[Tomatillo Green Chili Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Lettuce]]",
                "[Tomatillo Green Chili Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Tomatillo Green Chili Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Sour Cream, Lettuce]]",
                "[Tomatillo Green Chili Salsa, [Fajita Vegetables, Rice, Black Beans]]",
                "[Tomatillo Green Chili Salsa, [Fajita Vegetables, Rice, Cheese, Guacamole, Lettuce]]",
                "[Tomatillo Green Chili Salsa, [Fajita Vegetables, Rice, Cheese, Lettuce]]",
                "[Tomatillo Green Chili Salsa, [Fajita Vegetables, Rice, Pinto Beans, Cheese, Guacamole, Lettuce]]",
                "[Tomatillo Green Chili Salsa, [Fajita Vegetables, Rice, Pinto Beans, Cheese, Sour Cream, Lettuce]]",
                "[Tomatillo Green Chili Salsa, [Fajita Vegetables, Rice, Sour Cream, Guacamole]]",
                "[Tomatillo Green Chili Salsa, [Fajita Vegetables, Rice]]",
                "[Tomatillo Green Chili Salsa, [Rice, Black Beans, Cheese, Guacamole]]",
                "[Tomatillo Green Chili Salsa, [Rice, Black Beans, Cheese, Lettuce]]",
                "[Tomatillo Green Chili Salsa, [Rice, Black Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Tomatillo Green Chili Salsa, [Rice, Black Beans, Cheese, Sour Cream, Lettuce]]",
                "[Tomatillo Green Chili Salsa, [Rice, Black Beans, Cheese, Sour Cream]]",
                "[Tomatillo Green Chili Salsa, [Rice, Black Beans, Cheese]]",
                "[Tomatillo Green Chili Salsa, [Rice, Black Beans, Guacamole, Lettuce]]",
                "[Tomatillo Green Chili Salsa, [Rice, Black Beans, Pinto Beans, Guacamole, Lettuce]]",
                "[Tomatillo Green Chili Salsa, [Rice, Black Beans, Sour Cream, Cheese]]",
                "[Tomatillo Green Chili Salsa, [Rice, Black Beans, Sour Cream, Lettuce]]",
                "[Tomatillo Green Chili Salsa, [Rice, Black Beans]]",
                "[Tomatillo Green Chili Salsa, [Rice, Cheese, Guacamole, Lettuce]]",
                "[Tomatillo Green Chili Salsa, [Rice, Cheese, Guacamole]]",
                "[Tomatillo Green Chili Salsa, [Rice, Cheese, Lettuce]]",
                "[Tomatillo Green Chili Salsa, [Rice, Cheese, Sour Cream, Guacamole]]",
                "[Tomatillo Green Chili Salsa, [Rice, Cheese, Sour Cream, Lettuce]]",
                "[Tomatillo Green Chili Salsa, [Rice, Cheese]]",
                "[Tomatillo Green Chili Salsa, [Rice, Fajita Vegetables, Black Beans, Sour Cream, Cheese, Lettuce]]",
                "[Tomatillo Green Chili Salsa, [Rice, Pinto Beans, Cheese, Lettuce]]",
                "[Tomatillo Green Chili Salsa, [Rice, Pinto Beans, Cheese, Sour Cream, Lettuce]]",
                "[Tomatillo Green Chili Salsa, [Rice, Pinto Beans, Sour Cream, Lettuce]]",
                "[Tomatillo Green Chili Salsa, [Rice, Sour Cream, Cheese, Guacamole]]",
                "[Tomatillo Green Chili Salsa, [Rice, Sour Cream, Guacamole, Lettuce]]",
                "[Tomatillo Green Chili Salsa, [Rice, Sour Cream, Guacamole]]",
                "[Tomatillo Green Chili Salsa, [Sour Cream, Cheese, Guacamole]]",
                "[Tomatillo Red Chili Salsa, Fajita Vegetables]",
                "[Tomatillo Red Chili Salsa, Rice]",
                "[Tomatillo Red Chili Salsa, [Black Beans, Cheese, Sour Cream]]",
                "[Tomatillo Red Chili Salsa, [Fajita Vegetables, Black Beans, Sour Cream, Cheese, Lettuce]]",
                "[Tomatillo Red Chili Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Guacamole, Lettuce]]",
                "[Tomatillo Red Chili Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Lettuce]]",
                "[Tomatillo Red Chili Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Tomatillo Red Chili Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Sour Cream, Lettuce]]",
                "[Tomatillo Red Chili Salsa, [Fajita Vegetables, Rice, Black Beans, Cheese, Sour Cream]]",
                "[Tomatillo Red Chili Salsa, [Fajita Vegetables, Rice, Cheese, Guacamole]]",
                "[Tomatillo Red Chili Salsa, [Rice, Black Beans, Cheese, Guacamole]]",
                "[Tomatillo Red Chili Salsa, [Rice, Black Beans, Cheese, Lettuce, Guacamole]]",
                "[Tomatillo Red Chili Salsa, [Rice, Black Beans, Cheese, Lettuce]]",
                "[Tomatillo Red Chili Salsa, [Rice, Black Beans, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Tomatillo Red Chili Salsa, [Rice, Black Beans, Cheese, Sour Cream, Lettuce]]",
                "[Tomatillo Red Chili Salsa, [Rice, Black Beans, Cheese, Sour Cream]]",
                "[Tomatillo Red Chili Salsa, [Rice, Black Beans, Pinto Beans, Cheese, Sour Cream, Lettuce]]",
                "[Tomatillo Red Chili Salsa, [Rice, Black Beans, Pinto Beans, Cheese, Sour Cream]]",
                "[Tomatillo Red Chili Salsa, [Rice, Black Beans, Sour Cream, Cheese, Lettuce, Guacamole]]",
                "[Tomatillo Red Chili Salsa, [Rice, Black Beans, Sour Cream, Cheese, Lettuce]]",
                "[Tomatillo Red Chili Salsa, [Rice, Black Beans, Sour Cream, Guacamole, Lettuce]]",
                "[Tomatillo Red Chili Salsa, [Rice, Black Beans]]",
                "[Tomatillo Red Chili Salsa, [Rice, Cheese, Lettuce]]",
                "[Tomatillo Red Chili Salsa, [Rice, Cheese, Sour Cream, Guacamole]]",
                "[Tomatillo Red Chili Salsa, [Rice, Cheese, Sour Cream, Lettuce]]",
                "[Tomatillo Red Chili Salsa, [Rice, Cheese]]",
                "[Tomatillo Red Chili Salsa, [Rice, Fajita Vegetables, Black Beans, Cheese, Lettuce, Guacamole]]",
                "[Tomatillo Red Chili Salsa, [Rice, Fajita Vegetables, Black Beans, Sour Cream, Cheese]]",
                "[Tomatillo Red Chili Salsa, [Rice, Fajita Vegetables, Sour Cream, Lettuce]]",
                "[Tomatillo Red Chili Salsa, [Rice, Pinto Beans, Cheese, Lettuce]]",
                "[Tomatillo Red Chili Salsa, [Rice, Pinto Beans, Guacamole, Lettuce]]",
                "[Tomatillo Red Chili Salsa, [Rice, Sour Cream, Cheese, Guacamole]]",
                "[Tomatillo Red Chili Salsa, [Sour Cream, Cheese, Guacamole, Lettuce, Black Beans, Rice]]",
                "[Tomatillo Red Chili Salsa]",
                "[Tomatillo-Green Chili Salsa (Medium), Black Beans]",
                "[Tomatillo-Green Chili Salsa (Medium), [Black Beans, Rice, Cheese, Sour Cream]]",
                "[Tomatillo-Green Chili Salsa (Medium), [Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[Tomatillo-Green Chili Salsa (Medium), [Fajita Veggies, Cheese, Guacamole, Lettuce]]",
                "[Tomatillo-Green Chili Salsa (Medium), [Pinto Beans, Black Beans, Rice, Fajita Veggies, Sour Cream, Lettuce]]",
                "[Tomatillo-Green Chili Salsa (Medium), [Pinto Beans, Rice, Fajita Veggies, Lettuce]]",
                "[Tomatillo-Green Chili Salsa (Medium), [Rice, Cheese, Sour Cream, Lettuce]]",
                "[Tomatillo-Red Chili Salsa (Hot), Rice]",
                "[Tomatillo-Red Chili Salsa (Hot), [Black Beans, Cheese, Lettuce]]",
                "[Tomatillo-Red Chili Salsa (Hot), [Black Beans, Rice, Cheese, Lettuce]]",
                "[Tomatillo-Red Chili Salsa (Hot), [Black Beans, Rice, Cheese, Sour Cream, Lettuce]]",
                "[Tomatillo-Red Chili Salsa (Hot), [Black Beans, Rice, Cheese, Sour Cream]]",
                "[Tomatillo-Red Chili Salsa (Hot), [Black Beans, Rice, Fajita Veggies, Cheese, Lettuce]]",
                "[Tomatillo-Red Chili Salsa (Hot), [Cheese, Lettuce, Rice, Black Beans]]",
                "[Tomatillo-Red Chili Salsa (Hot), [Cheese, Sour Cream, Fajita Veggies, Pinto Beans, Rice]]",
                "[Tomatillo-Red Chili Salsa (Hot), [Pinto Beans, Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                "[Tomatillo-Red Chili Salsa (Hot), [Pinto Beans, Cheese, Sour Cream, Rice, Fajita Veggies, Guacamole]]",
                "[Tomatillo-Red Chili Salsa (Hot), [Pinto Beans, Rice, Cheese, Sour Cream, Guacamole]]",
                "[Tomatillo-Red Chili Salsa (Hot), [Pinto Beans, Rice, Cheese, Sour Cream, Lettuce]]",
                "[Tomatillo-Red Chili Salsa (Hot), [Pinto Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Guacamole]]",
                "[Tomatillo-Red Chili Salsa (Hot), [Pinto Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                "[Tomatillo-Red Chili Salsa (Hot), [Pinto Beans, Rice, Fajita Veggies, Cheese, Sour Cream]]",
                "[Tomatillo-Red Chili Salsa (Hot), [Rice, Black Beans, Cheese, Lettuce]]",
                "[Tomatillo-Red Chili Salsa (Hot), [Rice, Cheese, Sour Cream, Lettuce]]",
                "[Tomatillo-Red Chili Salsa (Hot), [Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                "[Tomatillo-Red Chili Salsa (Hot), [Rice, Fajita Veggies, Pinto Beans, Sour Cream, Cheese, Guacamole]]",
                "[Tomatillo-Red Chili Salsa (Hot), [Rice, Fajita Veggies, Sour Cream, Lettuce]]",
                "[Tomatillo-Red Chili Salsa (Hot), [Rice, Pinto Beans, Fajita Veggies, Cheese, Sour Cream, Guacamole]]",
                "[Tomatillo-Red Chili Salsa (Hot), [Sour Cream, Cheese, Fajita Veggies, Guacamole, Rice, Pinto Beans]]",
                "[White Rice]",
                "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium), Tomatillo-Red Chili Salsa (Hot)], [Black Beans, Rice, Cheese, Sour Cream]]",
                "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium), Tomatillo-Red Chili Salsa (Hot)], [Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium), Tomatillo-Red Chili Salsa (Hot)], [Pinto Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium)], [Black Beans, Cheese, Guacamole]]",
                "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium)], [Black Beans, Guacamole]]",
                "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium)], [Black Beans, Rice, Cheese, Sour Cream, Lettuce]]",
                "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium)], [Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium)], [Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium)], [Black Beans, Rice, Fajita Veggies, Lettuce]]",
                "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium)], [Fajita Veggies, Rice]]",
                "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium)], [Rice, Cheese, Sour Cream, Guacamole, Lettuce]]",
                "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium)], [Rice, Fajita Veggies]]",
                "[[Fresh Tomato Salsa (Mild), Roasted Chili Corn Salsa (Medium)], [Rice, Sour Cream, Guacamole, Lettuce]]",
                "[[Fresh Tomato Salsa (Mild), Tomatillo-Green Chili Salsa (Medium), Roasted Chili Corn Salsa (Medium), Tomatillo-Red Chili Salsa (Hot)], [Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream]]",
                "[[Fresh Tomato Salsa (Mild), Tomatillo-Green Chili Salsa (Medium), Roasted Chili Corn Salsa (Medium)], [Black Beans, Rice, Fajita Veggies, Cheese, Guacamole, Lettuce]]",
                "[[Fresh Tomato Salsa (Mild), Tomatillo-Green Chili Salsa (Medium)], [Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                "[[Fresh Tomato Salsa (Mild), Tomatillo-Red Chili Salsa (Hot)], [Black Beans, Rice, Cheese, Sour Cream, Guacamole]]",
                "[[Fresh Tomato Salsa (Mild), Tomatillo-Red Chili Salsa (Hot)], [Black Beans, Rice, Cheese, Sour Cream]]",
                "[[Fresh Tomato Salsa (Mild), Tomatillo-Red Chili Salsa (Hot)], [Guacamole, Rice, Black Beans, Cheese, Sour Cream]]",
                "[[Roasted Chili Corn Salsa (Medium), Fresh Tomato Salsa (Mild)], [Fajita Veggies, Lettuce, Rice, Black Beans]]",
                "[[Roasted Chili Corn Salsa (Medium), Fresh Tomato Salsa (Mild)], [Rice, Black Beans, Fajita Veggies, Cheese, Sour Cream, Guacamole]]",
                "[[Roasted Chili Corn Salsa (Medium), Fresh Tomato Salsa (Mild)], [Rice, Black Beans, Fajita Veggies, Guacamole]]",
                "[[Roasted Chili Corn Salsa (Medium), Fresh Tomato Salsa (Mild)], [Rice, Fajita Veggies]]",
                "[[Roasted Chili Corn Salsa (Medium), Tomatillo-Green Chili Salsa (Medium)], [Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                "[[Roasted Chili Corn Salsa (Medium), Tomatillo-Red Chili Salsa (Hot), Fresh Tomato Salsa (Mild), Tomatillo-Green Chili Salsa (Medium)], [Rice, Black Beans, Fajita Veggies, Cheese, Sour Cream]]",
                "[[Roasted Chili Corn Salsa (Medium), Tomatillo-Red Chili Salsa (Hot), Fresh Tomato Salsa (Mild)], [Rice, Pinto Beans, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                "[[Roasted Chili Corn Salsa (Medium), Tomatillo-Red Chili Salsa (Hot)], [Black Beans, Rice, Fajita Veggies, Guacamole, Lettuce]]",
                "[[Roasted Chili Corn Salsa (Medium), Tomatillo-Red Chili Salsa (Hot)], [Pinto Beans, Rice, Fajita Veggies, Cheese, Lettuce]]",
                "[[Roasted Chili Corn Salsa (Medium), Tomatillo-Red Chili Salsa (Hot)], [Pinto Beans, Rice, Fajita Veggies, Cheese]]",
                "[[Roasted Chili Corn Salsa (Medium), Tomatillo-Red Chili Salsa (Hot)], [Pinto Beans, Rice, Fajita Veggies]]",
                "[[Tomatillo-Green Chili Salsa (Medium), Roasted Chili Corn Salsa (Medium)], [Black Beans, Rice, Cheese, Sour Cream, Lettuce]]",
                "[[Tomatillo-Green Chili Salsa (Medium), Roasted Chili Corn Salsa (Medium)], [Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Guacamole]]",
                "[[Tomatillo-Green Chili Salsa (Medium), Roasted Chili Corn Salsa (Medium)], [Black Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Lettuce]]",
                "[[Tomatillo-Green Chili Salsa (Medium), Roasted Chili Corn Salsa (Medium)], [Rice, Fajita Veggies, Guacamole, Lettuce]]",
                "[[Tomatillo-Green Chili Salsa (Medium), Tomatillo-Red Chili Salsa (Hot)], [Pinto Beans, Rice, Fajita Veggies, Cheese, Sour Cream, Guacamole]]",
                "[[Tomatillo-Red Chili Salsa (Hot), Tomatillo-Green Chili Salsa (Medium)], [Rice, Black Beans, Cheese, Lettuce]]",
                "[[Tomatillo-Red Chili Salsa (Hot), Tomatillo-Green Chili Salsa (Medium)], [Rice, Black Beans, Lettuce]]"
            ],
            "selectedMode": "multiple",
            "show": true,
            "left": "center",
            "top": "top",
            "orient": "horizontal",
            "textStyle": {
                "fontSize": 12
            }
        }
    ],
    "animation": true,
    "color": [
        "#c23531",
        "#2f4554",
        "#61a0a8",
        "#d48265",
        "#749f83",
        "#ca8622",
        "#bda29a",
        "#6e7074",
        "#546570",
        "#c4ccd3",
        "#f05b72",
        "#ef5b9c",
        "#f47920",
        "#905a3d",
        "#fab27b",
        "#2a5caa",
        "#444693",
        "#726930",
        "#b2d235",
        "#6d8346",
        "#ac6767",
        "#1d953f",
        "#6950a1",
        "#918597",
        "#f6f5ec"
    ]
};
myChart_b5d5ec4525ae4c58a09a4cafc51f70ac.setOption(option_b5d5ec4525ae4c58a09a4cafc51f70ac);

