var myChart_a248c807d3524aa1a43a564fcedb102e = echarts.init(document.getElementById('a248c807d3524aa1a43a564fcedb102e'), 'light', {renderer: 'canvas'});

var option_a248c807d3524aa1a43a564fcedb102e = {
    "title": [
        {
            "text": "\u997c\u56fe\u793a\u4f8b",
            "left": "auto",
            "top": "auto",
            "textStyle": {
                "fontSize": 18
            },
            "subtextStyle": {
                "fontSize": 12
            }
        }
    ],
    "toolbox": {
        "show": true,
        "orient": "vertical",
        "left": "95%",
        "top": "center",
        "feature": {
            "saveAsImage": {
                "show": true,
                "title": "save as image"
            },
            "restore": {
                "show": true,
                "title": "restore"
            },
            "dataView": {
                "show": true,
                "title": "data view"
            }
        }
    },
    "series_id": 5174846,
    "tooltip": {
        "trigger": "item",
        "triggerOn": "mousemove|click",
        "axisPointer": {
            "type": "line"
        },
        "textStyle": {
            "fontSize": 14
        },
        "backgroundColor": "rgba(50,50,50,0.7)",
        "borderColor": "#333",
        "borderWidth": 0
    },
    "series": [
        {
            "type": "pie",
            "name": "\u6bcf\u79cd\u5546\u54c1\u9500\u552e\u91d1\u989d\u5728\u6d41\u6c34\u603b\u989d\u4e2d\u6240\u5360\u7684\u6bd4\u4f8b",
            "data": [
                {
                    "name": "6 Pack Soft Drink",
                    "value": 369.93000000000023
                },
                {
                    "name": "Barbacoa Bowl",
                    "value": 672.3600000000001
                },
                {
                    "name": "Barbacoa Burrito",
                    "value": 894.7500000000002
                },
                {
                    "name": "Barbacoa Crispy Tacos",
                    "value": 138.71
                },
                {
                    "name": "Barbacoa Salad Bowl",
                    "value": 106.4
                },
                {
                    "name": "Barbacoa Soft Tacos",
                    "value": 250.46
                },
                {
                    "name": "Bottled Water",
                    "value": 649.1799999999997
                },
                {
                    "name": "Bowl",
                    "value": 74.0
                },
                {
                    "name": "Burrito",
                    "value": 44.4
                },
                {
                    "name": "Canned Soda",
                    "value": 191.84000000000032
                },
                {
                    "name": "Canned Soft Drink",
                    "value": 603.75
                },
                {
                    "name": "Carnitas Bowl",
                    "value": 830.7100000000002
                },
                {
                    "name": "Carnitas Burrito",
                    "value": 616.33
                },
                {
                    "name": "Carnitas Crispy Tacos",
                    "value": 95.94
                },
                {
                    "name": "Carnitas Salad",
                    "value": 8.99
                },
                {
                    "name": "Carnitas Salad Bowl",
                    "value": 66.34
                },
                {
                    "name": "Carnitas Soft Tacos",
                    "value": 375.94
                },
                {
                    "name": "Chicken Bowl",
                    "value": 8044.629999999975
                },
                {
                    "name": "Chicken Burrito",
                    "value": 6387.059999999984
                },
                {
                    "name": "Chicken Crispy Tacos",
                    "value": 524.1100000000001
                },
                {
                    "name": "Chicken Salad",
                    "value": 81.09
                },
                {
                    "name": "Chicken Salad Bowl",
                    "value": 1506.25
                },
                {
                    "name": "Chicken Soft Tacos",
                    "value": 1199.0100000000002
                },
                {
                    "name": "Chips",
                    "value": 580.3399999999982
                },
                {
                    "name": "Chips and Fresh Tomato Salsa",
                    "value": 1033.9600000000003
                },
                {
                    "name": "Chips and Guacamole",
                    "value": 2475.6199999999963
                },
                {
                    "name": "Chips and Mild Fresh Tomato Salsa",
                    "value": 3.0
                },
                {
                    "name": "Chips and Roasted Chili Corn Salsa",
                    "value": 73.75000000000003
                },
                {
                    "name": "Chips and Roasted Chili-Corn Salsa",
                    "value": 43.02
                },
                {
                    "name": "Chips and Tomatillo Green Chili Salsa",
                    "value": 144.55
                },
                {
                    "name": "Chips and Tomatillo Red Chili Salsa",
                    "value": 159.3
                },
                {
                    "name": "Chips and Tomatillo-Green Chili Salsa",
                    "value": 88.43
                },
                {
                    "name": "Chips and Tomatillo-Red Chili Salsa",
                    "value": 93.21
                },
                {
                    "name": "Crispy Tacos",
                    "value": 14.8
                },
                {
                    "name": "Izze",
                    "value": 67.8
                },
                {
                    "name": "Nantucket Nectar",
                    "value": 111.87
                },
                {
                    "name": "Salad",
                    "value": 14.8
                },
                {
                    "name": "Side of Chips",
                    "value": 290.67999999999984
                },
                {
                    "name": "Steak Bowl",
                    "value": 2479.8099999999995
                },
                {
                    "name": "Steak Burrito",
                    "value": 4236.12999999999
                },
                {
                    "name": "Steak Crispy Tacos",
                    "value": 375.32000000000005
                },
                {
                    "name": "Steak Salad",
                    "value": 35.66
                },
                {
                    "name": "Steak Salad Bowl",
                    "value": 391.1499999999997
                },
                {
                    "name": "Steak Soft Tacos",
                    "value": 554.5500000000001
                },
                {
                    "name": "Veggie Bowl",
                    "value": 901.9500000000002
                },
                {
                    "name": "Veggie Burrito",
                    "value": 1002.2700000000003
                },
                {
                    "name": "Veggie Crispy Tacos",
                    "value": 8.49
                },
                {
                    "name": "Veggie Salad",
                    "value": 50.940000000000005
                },
                {
                    "name": "Veggie Salad Bowl",
                    "value": 182.5
                },
                {
                    "name": "Veggie Soft Tacos",
                    "value": 90.94
                }
            ],
            "radius": [
                "0%",
                "75%"
            ],
            "center": [
                "50%",
                "50%"
            ],
            "label": {
                "normal": {
                    "show": true,
                    "position": "outside",
                    "textStyle": {
                        "fontSize": 12
                    },
                    "formatter": "{b}: {d}%"
                },
                "emphasis": {
                    "show": true,
                    "textStyle": {
                        "fontSize": 12
                    },
                    "formatter": "{b}: {d}%"
                }
            },
            "seriesId": 5174846
        }
    ],
    "legend": [
        {
            "data": [
                "6 Pack Soft Drink",
                "Barbacoa Bowl",
                "Barbacoa Burrito",
                "Barbacoa Crispy Tacos",
                "Barbacoa Salad Bowl",
                "Barbacoa Soft Tacos",
                "Bottled Water",
                "Bowl",
                "Burrito",
                "Canned Soda",
                "Canned Soft Drink",
                "Carnitas Bowl",
                "Carnitas Burrito",
                "Carnitas Crispy Tacos",
                "Carnitas Salad",
                "Carnitas Salad Bowl",
                "Carnitas Soft Tacos",
                "Chicken Bowl",
                "Chicken Burrito",
                "Chicken Crispy Tacos",
                "Chicken Salad",
                "Chicken Salad Bowl",
                "Chicken Soft Tacos",
                "Chips",
                "Chips and Fresh Tomato Salsa",
                "Chips and Guacamole",
                "Chips and Mild Fresh Tomato Salsa",
                "Chips and Roasted Chili Corn Salsa",
                "Chips and Roasted Chili-Corn Salsa",
                "Chips and Tomatillo Green Chili Salsa",
                "Chips and Tomatillo Red Chili Salsa",
                "Chips and Tomatillo-Green Chili Salsa",
                "Chips and Tomatillo-Red Chili Salsa",
                "Crispy Tacos",
                "Izze",
                "Nantucket Nectar",
                "Salad",
                "Side of Chips",
                "Steak Bowl",
                "Steak Burrito",
                "Steak Crispy Tacos",
                "Steak Salad",
                "Steak Salad Bowl",
                "Steak Soft Tacos",
                "Veggie Bowl",
                "Veggie Burrito",
                "Veggie Crispy Tacos",
                "Veggie Salad",
                "Veggie Salad Bowl",
                "Veggie Soft Tacos"
            ],
            "selectedMode": "multiple",
            "show": true,
            "left": "center",
            "top": "top",
            "orient": "horizontal",
            "textStyle": {
                "fontSize": 12
            }
        }
    ],
    "animation": true,
    "color": [
        "#c23531",
        "#2f4554",
        "#61a0a8",
        "#d48265",
        "#749f83",
        "#ca8622",
        "#bda29a",
        "#6e7074",
        "#546570",
        "#c4ccd3",
        "#f05b72",
        "#ef5b9c",
        "#f47920",
        "#905a3d",
        "#fab27b",
        "#2a5caa",
        "#444693",
        "#726930",
        "#b2d235",
        "#6d8346",
        "#ac6767",
        "#1d953f",
        "#6950a1",
        "#918597",
        "#f6f5ec"
    ]
};
myChart_a248c807d3524aa1a43a564fcedb102e.setOption(option_a248c807d3524aa1a43a564fcedb102e);


