var myChart_40cb19c065064b27a8dcaec87fedead8 = echarts.init(document.getElementById('40cb19c065064b27a8dcaec87fedead8'), 'light', {renderer: 'canvas'});

var option_40cb19c065064b27a8dcaec87fedead8 = {
    "title": [
        {
            "text": "\u5546\u54c1\u9500\u552e\u6570\u91cf",
            "left": "auto",
            "top": "auto",
            "textStyle": {
                "fontSize": 18
            },
            "subtextStyle": {
                "fontSize": 12
            }
        }
    ],
    "toolbox": {
        "show": true,
        "orient": "vertical",
        "left": "95%",
        "top": "center",
        "feature": {
            "saveAsImage": {
                "show": true,
                "title": "save as image"
            },
            "restore": {
                "show": true,
                "title": "restore"
            },
            "dataView": {
                "show": true,
                "title": "data view"
            }
        }
    },
    "series_id": 1010325,
    "tooltip": {
        "trigger": "item",
        "triggerOn": "mousemove|click",
        "axisPointer": {
            "type": "line"
        },
        "textStyle": {
            "fontSize": 14
        },
        "backgroundColor": "rgba(50,50,50,0.7)",
        "borderColor": "#333",
        "borderWidth": 0
    },
    "series": [
        {
            "type": "bar",
            "name": "\u5546\u5bb6",
            "data": [
                55.0,
                66.0,
                91.0,
                12.0,
                10.0,
                25.0,
                211.0,
                4.0,
                6.0,
                126.0,
                351.0,
                71.0,
                60.0,
                8.0,
                1.0,
                6.0,
                40.0,
                761.0,
                591.0,
                50.0,
                9.0,
                123.0,
                120.0,
                230.0,
                130.0,
                506.0,
                1.0,
                23.0,
                18.0,
                45.0,
                50.0,
                33.0,
                25.0,
                2.0,
                20.0,
                29.0,
                2.0,
                110.0,
                221.0,
                386.0,
                36.0,
                4.0,
                31.0,
                56.0,
                87.0,
                97.0,
                1.0,
                6.0,
                18.0,
                8.0
            ],
            "barCategoryGap": "20%",
            "label": {
                "normal": {
                    "show": false,
                    "position": "top",
                    "textStyle": {
                        "fontSize": 12
                    }
                },
                "emphasis": {
                    "show": true,
                    "textStyle": {
                        "fontSize": 12
                    }
                }
            },
            "markPoint": {
                "data": []
            },
            "markLine": {
                "data": []
            },
            "seriesId": 1010325
        }
    ],
    "legend": [
        {
            "data": [
                "\u5546\u5bb6"
            ],
            "selectedMode": "multiple",
            "show": true,
            "left": "center",
            "top": "top",
            "orient": "horizontal",
            "textStyle": {
                "fontSize": 12
            }
        }
    ],
    "animation": true,
    "xAxis": [
        {
            "show": true,
            "nameLocation": "middle",
            "nameGap": 25,
            "nameTextStyle": {
                "fontSize": 14
            },
            "axisTick": {
                "alignWithLabel": false
            },
            "inverse": false,
            "boundaryGap": true,
            "type": "category",
            "splitLine": {
                "show": false
            },
            "axisLine": {
                "lineStyle": {
                    "width": 1
                }
            },
            "axisLabel": {
                "interval": "auto",
                "rotate": 0,
                "margin": 8,
                "textStyle": {
                    "fontSize": 12
                }
            },
            "data": [
                "6 Pack Soft Drink",
                "Barbacoa Bowl",
                "Barbacoa Burrito",
                "Barbacoa Crispy Tacos",
                "Barbacoa Salad Bowl",
                "Barbacoa Soft Tacos",
                "Bottled Water",
                "Bowl",
                "Burrito",
                "Canned Soda",
                "Canned Soft Drink",
                "Carnitas Bowl",
                "Carnitas Burrito",
                "Carnitas Crispy Tacos",
                "Carnitas Salad",
                "Carnitas Salad Bowl",
                "Carnitas Soft Tacos",
                "Chicken Bowl",
                "Chicken Burrito",
                "Chicken Crispy Tacos",
                "Chicken Salad",
                "Chicken Salad Bowl",
                "Chicken Soft Tacos",
                "Chips",
                "Chips and Fresh Tomato Salsa",
                "Chips and Guacamole",
                "Chips and Mild Fresh Tomato Salsa",
                "Chips and Roasted Chili Corn Salsa",
                "Chips and Roasted Chili-Corn Salsa",
                "Chips and Tomatillo Green Chili Salsa",
                "Chips and Tomatillo Red Chili Salsa",
                "Chips and Tomatillo-Green Chili Salsa",
                "Chips and Tomatillo-Red Chili Salsa",
                "Crispy Tacos",
                "Izze",
                "Nantucket Nectar",
                "Salad",
                "Side of Chips",
                "Steak Bowl",
                "Steak Burrito",
                "Steak Crispy Tacos",
                "Steak Salad",
                "Steak Salad Bowl",
                "Steak Soft Tacos",
                "Veggie Bowl",
                "Veggie Burrito",
                "Veggie Crispy Tacos",
                "Veggie Salad",
                "Veggie Salad Bowl",
                "Veggie Soft Tacos"
            ]
        }
    ],
    "yAxis": [
        {
            "show": true,
            "nameLocation": "middle",
            "nameGap": 25,
            "nameTextStyle": {
                "fontSize": 14
            },
            "axisTick": {
                "alignWithLabel": false
            },
            "inverse": false,
            "boundaryGap": true,
            "type": "value",
            "splitLine": {
                "show": true
            },
            "axisLine": {
                "lineStyle": {
                    "width": 1
                }
            },
            "axisLabel": {
                "interval": "auto",
                "formatter": "{value} ",
                "rotate": 0,
                "margin": 8,
                "textStyle": {
                    "fontSize": 12
                }
            }
        }
    ],
    "color": [
        "#c23531",
        "#2f4554",
        "#61a0a8",
        "#d48265",
        "#749f83",
        "#ca8622",
        "#bda29a",
        "#6e7074",
        "#546570",
        "#c4ccd3",
        "#f05b72",
        "#ef5b9c",
        "#f47920",
        "#905a3d",
        "#fab27b",
        "#2a5caa",
        "#444693",
        "#726930",
        "#b2d235",
        "#6d8346",
        "#ac6767",
        "#1d953f",
        "#6950a1",
        "#918597",
        "#f6f5ec"
    ],
    "dataZoom": [
        {
            "show": true,
            "type": "slider",
            "start": 50,
            "end": 100,
            "orient": "horizontal"
        }
    ]
};
myChart_40cb19c065064b27a8dcaec87fedead8.setOption(option_40cb19c065064b27a8dcaec87fedead8);
